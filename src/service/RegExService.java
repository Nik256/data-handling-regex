package service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExService {
    public String getAllWordsFromText(String text) {
        Pattern pattern = Pattern.compile("[\\w]+", Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(text);
        StringBuilder stringBuilder = new StringBuilder();

        while (matcher.find())
            stringBuilder.append(text.substring(matcher.start(), matcher.end()).toLowerCase()).append("\n");

        stringBuilder.setLength(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    public String getLongestWord(String text) {
        String[] words = getAllWordsFromText(text).split("\n");
        String longestWord = "";
        for (String word : words)
            if (word.length() > longestWord.length())
                longestWord = word;
        return longestWord;
    }

    public String getShortestWord(String text) {
        String[] words = getAllWordsFromText(text).split("\n");
        String shortestWord = words[0];
        for (String word : words)
            if (word.length() < shortestWord.length())
                shortestWord = word;
        return shortestWord;
    }

    public String getRidOfLocalPrefix(String str) {
        return str.replaceAll("(\\+7|8)\\s*\\(?3412\\)?", "");
    }

    public String replacePlaceholders(String text, String strKeys, String strValues) {
        String[] keys = strKeys.split(", ");
        String[] values = strValues.split(", ");

        for (int i = 0; i < keys.length; i++) {
            text = text.replaceAll("\\$" + keys[i], values[i]);
        }
        return text;
    }
}
