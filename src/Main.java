import demo.DemoService;
import service.RegExService;

public class Main {

    public static void main(String[] args) {
        new DemoService(new RegExService()).execute();
    }
}
