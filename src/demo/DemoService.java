package demo;

import service.RegExService;

public class DemoService {
    private RegExService regExService;

    public DemoService(RegExService regExService) {
        this.regExService = regExService;
    }

    public void execute() {
        String text = "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?";
        System.out.println(text);
        System.out.println(regExService.getAllWordsFromText(text));
        System.out.println("------------------------------------------------------------------------");

        System.out.println("Longest word: " + regExService.getLongestWord(text));
        System.out.println("Shortest word: " + regExService.getShortestWord(text));
        System.out.println("------------------------------------------------------------------------");

        String phone1 = "+7 (3412) 517-647";
        String phone2 = "8 (3412) 4997-12";
        String phone3 = "+7 3412 90-41-90";
        String phone4 = "8 3412 123-321";
        System.out.println(phone1 + " -> " + regExService.getRidOfLocalPrefix(phone1));
        System.out.println(phone2 + " -> " + regExService.getRidOfLocalPrefix(phone2));
        System.out.println(phone3 + " -> " + regExService.getRidOfLocalPrefix(phone3));
        System.out.println(phone4 + " -> " + regExService.getRidOfLocalPrefix(phone4));
        System.out.println("------------------------------------------------------------------------");

        String templateText = "Уважаемый, $userName, извещаем вас о том, что на вашем счете $accountNumber скопилась сумма," +
                " превышающая стоимость $monthsNumber месяцев пользования нашими услугами. Деньги продолжают поступать. " +
                "Вероятно, вы неправильно настроили автоплатеж. С уважением, $userSender $userPosition";
        String keys = "userName, accountNumber, monthsNumber, userSender, userPosition";
        String values = "John, 1231232, 10, Jane, Accountant";
        System.out.println(templateText);
        System.out.println(regExService.replacePlaceholders(templateText, keys, values));
    }
}
